/*
    Copyright © 2016 Zetok Zalbavar <zexavexxe@gmail.com>

    This file is part of gopull.

    gopull is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gopull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with gopull.  If not, see <http://www.gnu.org/licenses/>.
*/


//! Operations on a file.

use std::path::PathBuf;
use std::io::{self, Error, ErrorKind, Read};
use std::fs::File;


/// Name of the config file.
const CONFIG_NAME: &'static str = ".gopull";

/** Add `.gopull` to the `PathBuf` and check if it exists. If it does, return
`Some(PathBuf)`, none otherwise.
*/
fn config_in_workdir(workdir: &PathBuf) -> Option<PathBuf> {
    if workdir.is_file() { panic!("Why are you supplying file as directory?") }

    let path = workdir.join(CONFIG_NAME);
    if path.exists() {
        return Some(path)
    }
    None
}


/** Get location of the `.gopull` config file by traversing up the current
path.
*/
fn get_config_path() -> io::Result<PathBuf> {
    let mut path = PathBuf::from("./");

    // perhaps it's in the working dir
    if let Some(p) = config_in_workdir(&path) {
        return Ok(p)
    }

    // traverse up, perhaps it's somewhere up there
    while path.pop() {
        if let Some(p) = config_in_workdir(&path) {
            return Ok(p)
        }
    }

    Err(Error::new(ErrorKind::NotFound, "Config file not found"))
}

/** Get `String` from the contents of config file. `String` should be given to
the TOML parser.
*/
pub fn get_config_string() -> io::Result<String> {
    let mut string = String::new();

    let mut file = try!(File::open(try!(get_config_path())));

    if try!(file.read_to_string(&mut string)) == 0 {
        return Err(Error::new(ErrorKind::UnexpectedEof,
                              "Config file has no data in it"))
    }

    Ok(string)
}
