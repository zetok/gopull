/*
    Copyright © 2016 Zetok Zalbavar <zexavexxe@gmail.com>

    This file is part of gopull.

    gopull is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gopull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with gopull.  If not, see <http://www.gnu.org/licenses/>.
*/


//! Get config from the provided bytes TOML file, or propagate the error
//! message.



use std::io::{self, Error, ErrorKind};

use toml::{self, Value};

use super::file_ops::get_config_string;


/** All config options
*/
pub struct Config {
    /// Repo data.
    pub repository: Repository
}

impl Config {
    /// Get `Config` from the config file.
    pub fn load() -> io::Result<Config> {
        get_config_string()
            .and_then(|s| Repository::from_toml(&s))
            .map(|r| Config { repository: r })
    }
}

        
/// Repository-specific config.        
#[derive(Debug, Default, Eq, PartialEq)]
pub struct Repository {
    /// Name of the repository owner/org.
    pub owner: String,
    /// Name of the repository.
    pub name: String,
}

impl Repository {
    /// Supply `&str` with contents of the TOML config file.
    pub fn from_toml(string: &str) -> io::Result<Self> {
        //let config = match toml::Parser::new(string).parse() {
        //    Some(c) => c,
        //    None => return Repository::default(),
        //};

        toml::Parser::new(string)
            .parse()
            .ok_or(Error::new(ErrorKind::InvalidInput,
                   "Config is not a valid TOML"))
            .and_then(|cfg| {
                let repo_table = cfg.get("repository");
                if let None = repo_table {
                    return Err(Error::new(ErrorKind::InvalidData,
                        "There is no defined `repository` in the config"))
                }
                let repo_table = repo_table.expect("Repo table");

                match *repo_table {
                    Value::Table(_) => {},
                    _ => return Err(Error::new(ErrorKind::InvalidData,
                        "`repository` is not a table"))
                }
                let repo_table = repo_table.as_table().expect("↑");

                let maybe_owner = repo_table.get("owner");
                let maybe_name = repo_table.get("name");

                match (maybe_owner, maybe_name) {
                    (Some(vo), Some(vn)) => match (vo, vn) {
                        (&Value::String(ref o), &Value::String(ref n)) => 
                            Ok(Repository { owner: o.to_owned(), name: n.to_owned() }),
                        (_, &Value::String(_)) =>
                            Err(Error::new(ErrorKind::InvalidData,
                                "'owner' is supposed to be a string, but it's not")),
                        (&Value::String(_), _) =>
                            Err(Error::new(ErrorKind::InvalidData,
                                "'name' is supposed to be a string, but it's not")),
                        (_, _) =>
                            Err(Error::new(ErrorKind::InvalidData,
                                "'owner' and 'name' are supposed to be \
                                strings, but they're not")),
                    },
                    (None, Some(_)) =>
                        Err(Error::new(ErrorKind::InvalidData,
                            "There is no 'owner' key in the config")),
                    (Some(_), None) =>
                        Err(Error::new(ErrorKind::InvalidData,
                            "There is no 'name' key in the config")),
                    (None, None) =>
                        Err(Error::new(ErrorKind::InvalidData,
                            "There are no 'owner' and 'name' keys in the config")),
                }
            })
    }
}
