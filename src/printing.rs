/*
    Copyright © 2016 Zetok Zalbavar <zexavexxe@gmail.com>

    This file is part of gopull.

    gopull is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gopull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with gopull.  If not, see <http://www.gnu.org/licenses/>.
*/


//! Printing stuff.

use hubcaps::rep::*;

/// Print supplied pull requests.
pub fn print_prs(pulls: &[Pull]) {
    println!("Username | Branch | Link | Title");

    for pr in pulls {
        println!("\
            {}\t\
            {}\t\
            {}\t\
            {}",

            pr.user.login,
            pr.head.label,
            pr.html_url,
            pr.title
        );
    }
}
