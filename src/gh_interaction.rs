/*
    Copyright © 2016 Zetok Zalbavar <zexavexxe@gmail.com>

    This file is part of gopull.

    gopull is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gopull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with gopull.  If not, see <http://www.gnu.org/licenses/>.
*/

//! Module for interacting with github stuff.

use hyper::Client;
use hubcaps::{Credentials, Github, State};
use hubcaps::rep::*;

/// Provide list of pull request for given repo.
pub fn list_open_pulls(repo_owner: String, repo_name: String) -> Vec<Pull> {

    // proxying doesn't work, a hyper bug with HTTPS over proxy
    let client = Client::new();//with_http_proxy("127.0.0.1", 8118);
    let github = Github::new(
        "gopull-user-agent/0.0.0",
        &client,
        Credentials::None
    );

    let repo = github.repo(repo_owner, repo_name);
    let pulls = repo.pulls();
    let get_prs_opts = PullListOptionsBuilder::new()
        .state(State::Open)
        .build();

    pulls.list(&get_prs_opts).expect("Failed to get list of PRs")
}
