/*
    Copyright © 2016 Zetok Zalbavar <zexavexxe@gmail.com>

    This file is part of gopull.

    gopull is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gopull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with gopull.  If not, see <http://www.gnu.org/licenses/>.
*/


extern crate hubcaps;
extern crate hyper;
extern crate toml;

use std::process::exit;

#[warn(missing_docs)]
pub mod config;
#[warn(missing_docs)]
pub mod file_ops;
#[warn(missing_docs)]
pub mod gh_interaction;
#[warn(missing_docs)]
pub mod printing;

use config::Config;
use gh_interaction::*;
use printing::*;



fn main() {
    let config = Config::load();
    if let Err(e) = config {
        println!("Something went wrong.\n\n{}", e);
        exit(1)
    }
    let config = config.expect("Look above, Luke");

    print_prs(&list_open_pulls(config.repository.owner, config.repository.name));
}
