# gopull

This application fetches information about open pull requests in a given
repository.

Config file must be named `.gopull` and be located in current directory, or one
of directories above it.

Example `.gopull` config:

```toml
[repository]
owner = "qTox"
name = "qTox"
```


# Dependencies

You'll need [Rust] >= 1.11.0.

When you'll have it, build debug version with

# Building

```bash
cargo build
```

`gopull` binary will be located in the `target/debug/`

## Optimized build

```bash
cargo build --release
```

Optimized `gopull` binary will be located in the `target/release/`

## License

Licensed under GPLv3+. For details, see [COPYING](/COPYING).

[Rust]: https://www.rust-lang.org/
